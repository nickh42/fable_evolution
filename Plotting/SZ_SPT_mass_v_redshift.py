#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 15:27:21 2018
Plot halo mass versus redshift for SPT catalogue
@author: nh444
"""
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

h_scale = 0.6774
omega_m = 0.3089


def Ez(z):
    return np.sqrt(omega_m*(1.0+z)**3.0 + (1.0 - omega_m))


obs_dir = "/home/nh444/vault/ObsData/SZ/"
outdir = "/home/nh444/Documents/Fable_evol/"
#outdir = "/data/curie4/nh444/project1/L-T_relations/"
col = ['#24439b', '#ED7600', '#50BFD7']
col = ['#068587', '#4FB99F', '#F2B134']

#SPT = np.genfromtxt(obs_dir+"SPT/SPT-SZ.csv", delimiter=",")
#z = SPT[:, 8]
#M = SPT[:, 12]*(0.7/h_scale) ## fiducial cosmology (h=0.7 omega_m=0.3)
##M = SPT[:, 14]*(0.67/h_scale) ## Planck cosmology (h=0.67 omega_m=0.32)
##M = SPT[:, 16]*(0.734/h_scale) ## dynamical masses for cosmology with h=0.734 omega_m=0.45)
SPT = np.genfromtxt(obs_dir+"SPT/SPT-SZ_WL.csv", delimiter=",")
z = SPT[:, 8]
M = SPT[:, 14] * 1e14 * (0.7/h_scale)

fig, ax = plt.subplots(figsize=(7, 7))
ax.set_yscale('log')
ax.set_ylabel(r"M$_{500}$ [M$_{\odot}$]")
ax.set_ylim(1e14, 3e15)
ax.set_xlabel("Redshift")
ax.set_xlim(0, 2)
ax.plot(z, M, marker='o', ms=9, mec='none', c='0.75', alpha=0.5,
        ls='none', label="SPT-SZ")

# Plot lines of different E(z) dependence
zlist = np.linspace(0.1, 1.8, num=10)
norm = 3.5e14
ax.plot(zlist, norm*Ez(zlist)**-0.0, lw=4, ls='--', c=col[0],
        label=r"$\gamma = 0.0$")
ax.plot(zlist, norm*Ez(zlist)**-0.5, lw=4, ls='--', c=col[1],
        label=r"$\gamma = -0.5$")
ax.plot(zlist, norm*Ez(zlist)**-1.0, lw=4, ls='--', c=col[2],
        label=r"$\gamma = -1.0$")
ax.legend(borderaxespad=1)
ax.xaxis.set_major_locator(ticker.MultipleLocator(0.5))

plt.tight_layout(pad=0.4)
fig.savefig(outdir+"SZ_SPT_mass_v_redshift.pdf")
print "Output to", outdir+"SZ_SPT_mass_v_redshift.pdf"
