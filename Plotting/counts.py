#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 17:15:53 2018

@author: nh444
"""
import numpy as np
import powspec_python.compare_counts as cc

outdir = "/home/nh444/Documents/Fable_evol/"
outdir = "/data/curie4/nh444/project1/L-T_relations/"

# Use nz shells in redshift between z1 and z2.
z1 = 0.0
z2 = 1.8
nz = 20
# Survey area in degrees-squared.
survey_area = 2500.0  # SPT-SZ and SPT-3G
# Fraction of the sky spanned by survey_area
sky_fraction = (survey_area / (4.0*np.pi*(180.0/np.pi)**2))

# =============================================================================
# # Plot only the biased MACSIS relation.
# cc.plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
#                M_func=cc.get_Mthresh_weights, have_weights=True,
#                M_func_kwargs={'Y500': 3.75e-5, 'Epow': 0.25,
#                               'h_scale': 0.6774},
#                Delta=500.0, Delta_type='crit',
#                labels=["FABLE", "MACSIS", "Planelles+17",
#                        "Nagarajan+18", "Andersson+11"],
#                ylims=[0.2, 200],
#                show_Ntot_z=[1.0, 1.5, 1.8], plot_Mthresh=True, plot_SPT=True,
#                outdir=outdir, outsuffix="_HighThresh",
#                title="High detection threshold",
#                verbose=True)
# 
# cc.plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
#                M_func=cc.get_Mthresh_weights, have_weights=True,
#                M_func_kwargs={'Y500': 1.1e-5, 'Epow': 0.25,
#                               'h_scale': 0.6774},
#                leg_kwargs={'borderaxespad': 2.5},
#                Delta=500.0, Delta_type='crit',
#                labels=["FABLE", "MACSIS", "Planelles+17",
#                        "Nagarajan+18", "Andersson+11"],
#                ylims=[10, 1500],
#                plot_SPT3G=False, plot_CMBS4=False,
#                show_Ntot_z=[1.0, 1.5, 1.8], plot_Mthresh=True,
#                outdir=outdir, outsuffix="_LowThresh",
#                title="Low detection threshold",
#                verbose=True)
# =============================================================================

# Plot the biased and (approx) unbiased MACSIS relation.
labels = ["FABLE", "MACSIS", "MACSIS BC", "Planelles+17",
          "Nagarajan+18", "Andersson+11"]
col = ['#24439b', '#ED7600', '#B94400', '#50BFD7', '0.3', '0.6']
ls = ['-', '-', '-', '-', '-', '--']

cc.plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
               M_func=cc.get_Mthresh_weights, have_weights=True,
               M_func_kwargs={'Y500': 3.75e-5, 'Epow': 0.25,
                              'unbiased_MACSIS': True, 'h_scale': 0.6774},
               leg_kwargs={'loc': 'lower left',
                           'bbox_to_anchor': (0.06, 0.03),
                           'borderaxespad': 0.0},
               Delta=500.0, Delta_type='crit',
               labels=labels, col=col, ls=ls,
               ylims=[0.2, 200],
               show_Ntot_z=[1.0, 1.5, 1.8], plot_Mthresh=True, plot_SPT=True,
               outdir=outdir, outsuffix="_HighThresh_incl_unbiased",
               title="High detection threshold",
               verbose=True)

cc.plot_counts(z1=z1, z2=z2, nz=nz, sky_fraction=sky_fraction,
               M_func=cc.get_Mthresh_weights, have_weights=True,
               M_func_kwargs={'Y500': 1.1e-5, 'Epow': 0.25,
                              'unbiased_MACSIS': True, 'h_scale': 0.6774},
               leg_kwargs={'loc': 'lower left',
                           'bbox_to_anchor': (0.12, 0.06),
                           'borderaxespad': 0.0},
               Delta=500.0, Delta_type='crit',
               labels=labels, col=col, ls=ls,
               ylims=[2, 2000],
               plot_SPT3G=False, plot_CMBS4=False,
               show_Ntot_z=[1.0, 1.5, 1.8], plot_Mthresh=True,
               outdir=outdir, outsuffix="_LowThresh_incl_unbiased",
               title="Low detection threshold",
               verbose=True)
