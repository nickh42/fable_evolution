#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 13:28:40 2019

@author: nh444
"""
import numpy as np
import scaling.plotting.plot_relation as pr

outdir = "/data/curie4/nh444/project1/L-T_relations/"

basedir = "/data/curie4/nh444/project1/L-T_relations/"
simdirs = ["L40_512_MDRIntRadioEff",
           ]
labels = [None,
          ]
zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
          "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
          "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
          "c512_MDRInt",
          "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
          "c240_MDRInt", "c272_MDRInt", "c304_MDRInt",
          "c336_MDRInt",
          "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
          "c464_MDRInt",
          "c496_MDRInt_SUBF",
          ]]

# cosmology to scale to:
h_scale = 0.6774
omega_m = 0.3089
omega_l = 0.6911

kwargs = {'labels': labels,
          'zoomdirs': zoomdirs, 'zooms': zooms,
          'h_scale': h_scale, 'omega_m': omega_m, 'omega_l': omega_l,
          'basedir': basedir, 'outdir': outdir,
          'do_fit': False,
          'fit_method': 'bces_orthog',
          'Ntrials': 10000, 'fit_lw': 3, 'fit_alpha': 1.0,
          'M500min_plot': 3e13, 'M500min_fit': 3e13, 'M_power_fit': 0.5,
          'verbose': False}

# ----- X-ray relations ----- #
inprefix = "L-T_0.5-10"
insuffix = "_exp1e9_Tvir4_metals_wabs"
# plot projected aperture values, otherwise 3D spherical.
kwargs['projected'] = True

# range of redshifts for plot_relation_redshift
minz = 0.0
maxz = 2.1
zstep = 0.2

fit_only = True  # only counts objects used in the fit

z_list = np.arange(minz, maxz, zstep)


def E(z): return (omega_m*(1+z)**3 + omega_l)**0.5


for i, z in enumerate(z_list):
    filename = inprefix+"_z"+"{:.1f}".format(z)+insuffix+".hdf5"
    results = pr.plot_relation(simdirs, [filename], ["T500", "gasmass"], z=z,
                               do_plot=False, **kwargs)
    result = results['result_0']
    if fit_only:
        ind = np.where(result['M'] * E(z)**kwargs['M_power_fit'] > kwargs['M500min_fit'])
        M_temp = result['M'][ind]
        T_temp = result['X'][ind]
        Mgas_temp = result['Y'][ind]
    else:
        M_temp = result['M']
        T_temp = result['X']
        Mgas_temp = result['Y']
    if i == 0:
        M = M_temp
        T = T_temp
        Mgas = Mgas_temp
    else:
        M = np.concatenate((M, M_temp))
        T = np.concatenate((T, T_temp))
        Mgas = np.concatenate((Mgas, Mgas_temp))

print "Median mass = {:.3f}".format(np.median(M) / 1e13), "e13 Msun"
print "Median temperature = {:.3f}".format(np.median(T))
print "Median gas mass = {:.3f}".format(np.median(Mgas) / 1e12), "e12 Msun"
print
print "Mean mass = {:.3f}".format(np.mean(M) / 1e13), "e13 Msun"
print "Mean temperature = {:.3f}".format(np.mean(T))
print "Mean gas mass = {:.3f}".format(np.mean(Mgas) / 1e12), "e12 Msun"
