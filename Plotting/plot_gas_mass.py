#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 11:38:08 2018

@author: nh444
"""
import ICM.ICM as icm
import numpy as np

h_scale = 0.6774

outdir = "/home/nh444/Documents/Fable_evol/"
# outdir = "/data/curie4/nh444/project1/L-T_relations/"

indir = "/data/curie4/nh444/project1/ICM/"
simdirs = ["L40_512_MDRIntRadioEff"]
labels = ["FABLE"]

zoomdirs = ["MDRInt/"]  # corresponding to each simdir
zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
          "c224_MDRInt", "c256_MDRInt", "c288_MDRInt",
          "c320_MDRInt",
          "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
          "c512_MDRInt",
          "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
          "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
          "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
          "c464_MDRInt",
          "c496_MDRInt_SUBF",
          ]]

col = ['#24439b', '#ED7600']  # blue orange
hlightcol = '#8292BF'
ms = 8
mew = 1.8
errlw = 1.4

#icm.plot_Mgas_vs_mass(indir, simdirs, zoomdirs, zooms, 0.0, labels=labels,
#                      M500lims=[5e12, 3e15], fractional=False,
#                      ylim=[3e11, 3e14],
#                      col=col, ms=ms, mew=mew, errlw=0, hlightcol=hlightcol,
#                      outdir=outdir, h_scale=h_scale)

redshifts = np.arange(0.0, 1.5, 0.2)
icm.plot_Mgas_vs_redshift(indir, simdirs, zoomdirs, zooms, redshifts,
                          M500lims=[2.9e14, 1e16], labels=labels,
                          col=col, outdir=outdir,
                          ms=ms, mew=mew, errlw=errlw, hlightcol=hlightcol,
                          h_scale=h_scale)
