def main():
    import scaling.plotting.plot_relation as pr

    outdir = "/home/nh444/Documents/Fable_evol/"
    outdir = "/data/curie4/nh444/project1/L-T_relations/"

    basedir = "/data/curie4/nh444/project1/L-T_relations/"
    simdirs = ["L40_512_MDRIntRadioEff",
               ]
    labels = [None,
              ]
    zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
    zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
              "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
              "c288_MDRInt", "c320_MDRInt", "c352_MDRInt",
              "c384_MDRInt", "c410_MDRInt",
              "c448_MDRInt", "c480_MDRInt", "c512_MDRInt"]]
    zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
              "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
              "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
              "c512_MDRInt",
              "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
              "c240_MDRInt", "c272_MDRInt", "c304_MDRInt",
              "c336_MDRInt",
              "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
              "c464_MDRInt",
              "c496_MDRInt_SUBF",
              ]]
    idxScat = range(len(simdirs))
    idxMed = []
    do_fit = True

    # cosmology to scale to:
    h_scale = 0.6774
    omega_m = 0.3089
    omega_l = 0.6911

    figsize = (7, 7)
    col = ['#24439b', '#ED7600']  # blue orange
    hlightcol = '#8292BF'
    ls = ['solid']*len(simdirs)
    markers = ['D', 's', 'o', '^', '>', 'v', '<']
    ms = 8
    lw = 3
    mew = 1.8
    errlw = 0  # line width of error bars for obs data
    binwidth = 0.1  # dex
    axlabels = None

    kwargs = {'axlabels': axlabels, 'labels': labels,
              'zoomdirs': zoomdirs, 'zooms': zooms,
              'h_scale': h_scale, 'omega_m': omega_m, 'omega_l': omega_l,
              'basedir': basedir, 'outdir': outdir, 'figsize': figsize,
              'idxScat': idxScat, 'idxMed': idxMed,
              'mec': col, 'hlightcol': hlightcol,
              'markers': markers, 'ls': ls, 'ms': ms, 'lw': lw, 'mew': mew,
              'errlw': errlw,
              'plot_obs': True,
              'xray_obs': {'Lieu16': True, 'Clerc14': False,
                           'Fassbender11': False},
              'binwidth': binwidth, 'do_fit': do_fit,
              'fit_method': 'bces_orthog', 'Ntrials': 10000,
              'fit_col': ['#ED8013'], 'fit_lw': 4, 'fit_alpha': 1.0,
              'M500min_plot': 0, 'M500min_fit': 3e13, 'M_power_fit': 0.5,
              'verbose': True}

    # ----- X-ray relations ----- #
    # Set filename prefixes and suffixes for the observation comparison
    # and the redshift evolution.
    inprefix_obs = "L-T_0.5-10"
    insuffix_obs = "_exp1e7_Tvir4_metals_wabs"
    inprefix_evol = "L-T_0.2-12"
    insuffix_evol = "_Athena_exp1e8_Tvir4_metals_wabs"
    # plot projected aperture values, otherwise 3D spherical.
    kwargs['projected'] = True

    # range of redshifts for plot_relation_redshift
    minz = 0.0
    maxz = 1.9
    zstep = 0.2
    plot_fits = True

# =============================================================================
#     # mass--temperature (spectroscopic and mass-weighted)
#     # Comparison with observations (spectroscopic)
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs+".hdf5"]
#     outname = "M-T_z0.4.pdf"
#     pr.plot_relation(simdirs, filenames, ["T500", "mass"], z_comp=0.4,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "M-T_z1.0.pdf"
#     pr.plot_relation(simdirs, filenames, ["T500", "mass"], z_comp=1.0,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
# =============================================================================
# =============================================================================
#     pr.plot_relation_redshift(simdirs, ["T500mw", "mass"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               outprefix=None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               do_plot=False, plot_fits=False)
#     # Plot parameters of both M-T and M-Tmw
#     pr.plot_relation_redshift(simdirs, ["T500", "mass"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               param_kwargs={'axnames2': ["T500mw", "mass"],
#                                             # 'normalise': False,
#                                             'add_titles': True,
#                                             'ylims_slope': [1.31, 2.07],
#                                             'tspacing': {'slope': 0.2,
#                                                          'norm': 0.1,
#                                                          'scat': 0.02}},
#                               outprefix="M-T" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               plot_fits=plot_fits)
# =============================================================================

# =============================================================================
#     # luminosity temperature (spectroscopic and mass-weighted)
#     # Comparison with observations (spectroscopic)
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs+".hdf5"]
#     outname = "L-T_z0.4.pdf"
#     pr.plot_relation(simdirs, filenames, ["T500", "lum"], z_comp=0.4,
#                      outname=outname, leg_len=3, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "L-T_z1.0.pdf"
#     pr.plot_relation(simdirs, filenames, ["T500", "lum"], z_comp=1.0,
#                      outname=outname, **kwargs)
# =============================================================================
# =============================================================================
#     # Get best-fitting parameters for L-Tmw without plotting.
#     pr.plot_relation_redshift(simdirs, ["T500mw", "lum"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               outprefix=None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               do_plot=False, plot_fits=False)
#     # Plot parameters of both L-T and L-Tmw
#     pr.plot_relation_redshift(simdirs, ["T500", "lum"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               param_kwargs={'axnames2': ["T500mw", "lum"],
#                                             'ylims_scat': [0.04, 0.36],
#                                             'ylims_norm': [-0.25, None],
#                                             'ylims_slope': [1.8, 4.4],
#                                             'tspacing': {'slope': 0.5,
#                                                          'norm': 0.2,
#                                                          'scat': 0.1}},
#                               outprefix="L-T" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               plot_fits=plot_fits)
# =============================================================================

# =============================================================================
#     # YX--total mass (spectroscopic and mass-weighted)
#     # Comparison with observations (spectroscopic)
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs+".hdf5"]
#     outname = "YX-M_z0.4.pdf"
#     pr.plot_relation(simdirs, filenames, ["mass", "YX"], z_comp=0.4,
#                      add_symbol_leg=True,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "YX-M_z1.0.pdf"
#     pr.plot_relation(simdirs, filenames, ["mass", "YX"], z_comp=1.0,
#                      outname=outname, **kwargs)
# =============================================================================
# =============================================================================
#     # Get best-fitting parameters for YXmw-M without plotting.
#     pr.plot_relation_redshift(simdirs, ["mass", "YXmw"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               outprefix=None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               do_plot=False, plot_fits=False)
#     # Plot parameters of both YX-M and YXmw-M
#     pr.plot_relation_redshift(simdirs, ["mass", "YX"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               param_kwargs={'axnames2': ["mass", "YXmw"],
#                                             'add_titles': True,
#                                             'ylims_scat': [0.03, 0.17],
#                                             'ylims_norm': [-0.1, None],
#                                             'tspacing': {'slope': 0.2,
#                                                          'norm': 0.1,
#                                                          'scat': 0.1}},
#                               outprefix="YX-M" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               plot_fits=plot_fits)
# =============================================================================

# =============================================================================
#     # luminosity--mass
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs+".hdf5"]
#     outname = "L-M_z0.4.pdf"
#     pr.plot_relation(simdirs, filenames, ["mass", "lum"], z_comp=0.4,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "L-M_z1.0.pdf"
#     pr.plot_relation(simdirs, filenames, ["mass", "lum"], z_comp=1.0,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
# =============================================================================
# =============================================================================
#     pr.plot_relation_redshift(simdirs, ["mass", "lum"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               outprefix="L-M" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               plot_fits=plot_fits,
#                               param_kwargs={'ylims_slope': [1.2, None],
#                                             'tspacing': {'slope': 0.4,
#                                                          'norm': 0.2,
#                                                          'scat': 0.1}})
# =============================================================================

# =============================================================================
#     # luminosity--gas mass
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs+".hdf5"]
#     outname = "L-Mgas_z0.4.pdf"
#     pr.plot_relation(simdirs, filenames, ["gasmass", "lum"], z_comp=0.4,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "L-Mgas_z1.0.pdf"
#     pr.plot_relation(simdirs, filenames, ["gasmass", "lum"], z_comp=1.0,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
# =============================================================================
# =============================================================================
#     pr.plot_relation_redshift(simdirs, ["gasmass", "lum_cex"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               param_kwargs={'tspacing': {'norm': 0.1,
#                                                          'scat': 0.05}},
#                               outprefix="L-Mgas" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               plot_fits=plot_fits)
# =============================================================================

# =============================================================================
#     # gas mass--total mass
# #    kwargs['fit_to_median'] = True
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs+".hdf5"]
#     outname = "Mgas-M_z0.4.pdf"
#     pr.plot_relation(simdirs, filenames, ["mass", "gasmass"], z_comp=0.4,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "Mgas-M_z1.0.pdf"
#     pr.plot_relation(simdirs, filenames, ["mass", "gasmass"], z_comp=1.0,
#                      mass_bias_arrow=True, outname=outname, **kwargs)
# =============================================================================
# =============================================================================
#     pr.plot_relation_redshift(simdirs, ["mass", "gasmass"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               param_kwargs={'ylims_slope': [0.95, 1.45],
#                                             'add_titles': True,
#                                             'tspacing': {'norm': 0.1,
#                                                          'scat': 0.05}},
#                               outprefix="Mgas-M" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               plot_fits=plot_fits)
# 
# =============================================================================
# =============================================================================
#     # YX (mass-weighted)--total mass
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs+".hdf5"]
#     outname = "YXmw-M_z0.4.pdf"
#     pr.plot_relation(simdirs, filenames, ["mass", "YXmw"], z=0.4,
#                      outname=outname, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "YXmw-M_z1.0.pdf"
#     pr.plot_relation(simdirs, filenames, ["mass", "YXmw"], z=1.0,
#                      outname=outname, **kwargs)
#     pr.plot_relation_redshift(simdirs, ["mass", "YXmw"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               outprefix="YXmw-M" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               param_kwargs={'ylims_scat': [None, 0.15]},
#                               plot_fits=plot_fits)
# 
#     # mass--mass-weighted temperature
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs+".hdf5"]
#     outname = "M-Tmw_z0.4.pdf"
#     kwargs['xray_obs']['Giles16'] = False
#     pr.plot_relation(simdirs, filenames, ["T500mw", "mass"], z=0.4,
#                      outname=outname, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "M-Tmw_z1.0.pdf"
#     kwargs['xray_obs']['Giles16'] = True
#     pr.plot_relation(simdirs, filenames, ["T500mw", "mass"], z=1.0,
#                      outname=outname, **kwargs)
#     pr.plot_relation_redshift(simdirs, ["T500mw", "mass"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               outprefix="M-Tmw" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               param_kwargs={'ylims_scat': [None, 0.12]},
#                               plot_fits=plot_fits)
# 
#     # luminosity--mass-weighted temperature
#     filenames = [inprefix_obs+"_z0.4"+insuffix_obs_obs+".hdf5"]
#     outname = "L-Tmw_z0.4.pdf"
#     pr.plot_relation(simdirs, filenames, ["T500mw", "lum"], z=0.4,
#                      outname=outname, **kwargs)
#     filenames = [inprefix_obs+"_z1.0"+insuffix_obs+".hdf5"]
#     outname = "L-Tmw_z1.0.pdf"
#     pr.plot_relation(simdirs, filenames, ["T500mw", "lum"], z=1.0,
#                      outname=outname, **kwargs)
#     pr.plot_relation_redshift(simdirs, ["T500mw", "lum"], kwargs,
#                               inprefix=inprefix_evol, insuffix=insuffix_evol,
#                               outprefix="L-Tmw" if plot_fits else None,
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               plot_fits=plot_fits,
#                               param_kwargs={'ylims_scat': [0.05, 0.4],
#                                             'ylims_slope': [1.5, None]})
# 
# =============================================================================
# =============================================================================
#     # ----- APPENDIX ----- #
#     kwargs['plot_obs'] = False
# 
#     # min mass dependence
#     col = ['#068587', '#4FB99F', '#F2B134']
# 
# #    z = 0.0
# #    filenames = [inprefix_evol+"_z"+"{:.1f}".format(z)+insuffix_evol+".hdf5"]
# #    pr.plot_fit_mass_dependence(simdirs, filenames, kwargs, z=z,
# #                                axes=["mass", "YXmw"],
# #                                outname="M-T_z"+"{:.1f}".format(z)+"_mass_dependence.pdf",
# #                                plotlabel="z $="+"{:.1f}".format(z)+"$",
# #                                outdir=outdir)
# 
#     # Mgas-M
#     pr.plot_fit_mass_dependence_params(simdirs, insuffix_evol, kwargs,
#                                        inprefix=inprefix_evol, axes=["mass", "gasmass"],
#                                        minz=minz, maxz=maxz, zstep=zstep,
#                                        param_kwargs={'ylims_slope': [0.95, 1.4],
#                                                      'add_titles': True,
#                                                      'tspacing': {'slope': 0.1,
#                                                                   'norm': 0.1,
#                                                                   'scat': 0.04},
#                                                      'min_num': 10,
#                                                      'col': col,
#                                                      'normalise': False},
#                                        outdir=outdir)
# 
#     # M-Tspec
# #    pr.plot_fit_mass_dependence_params(simdirs, insuffix_evol, kwargs, plot=False,
# #                                       inprefix=inprefix_evol,
# #                                       axes=["T500mw", "mass"],
# #                                       minz=minz, maxz=maxz, zstep=zstep)
# #    pr.plot_fit_mass_dependence_params(simdirs, insuffix_evol, kwargs,
# #                                       inprefix=inprefix_evol,
# #                                       axes=["T500", "mass"],
# #                                       minz=minz, maxz=maxz, zstep=zstep,
# #                                       param_kwargs={#'axnames2': ["T500mw", "mass"],
# #                                                     'min_num': 10,
# #                                                     'ylims_slope': [1.0, 2.3],
# #                                                     'tspacing': {'slope': 0.4,
# #                                                                  'norm': 0.1,
# #                                                                  'scat': 0.04},
# #                                                     },
# #                                       outdir=outdir)
# #    # M-Tmw
# #    pr.plot_fit_mass_dependence_params(simdirs, insuffix_evol, kwargs,
# #                                       inprefix=inprefix_evol,
# #                                       axes=["T500mw", "mass"],
# #                                       minz=minz, maxz=maxz, zstep=zstep,
# #                                       param_kwargs={'ylims_slope': [1.3, 2.3],
# #                                                     'tspacing': {'norm': 0.1,
# #                                                                  'scat': 0.02},
# #                                                     'min_num': 10,
# #                                                     'col': col,
# #                                                     'normalise': False},
# #                                       outdir=outdir)
# =============================================================================

# =============================================================================
# #    # SZ relation mass dependence.
# #    kwargs['basedir'] = "/data/curie4/nh444/project1/SZ/"
# #    kwargs['zoomdirs'] = ["/data/curie4/nh444/project1/SZ/MDRInt/"]
# #    kwargs['projected'] = False
# #    insuffix = "_r500"
# #    pr.plot_fit_mass_dependence_params(simdirs, insuffix, kwargs,
# #                                       inprefix="SZ-M500",
# #                                       axes=["mass", "Y500"],
# #                                       minz=minz, maxz=maxz, zstep=zstep,
# #                                       param_kwargs={'min_num': 10},
# #                                       outdir=outdir)
# 
#     # min mass evolution dependence
#     col = ['#068587', '#4FB99F', '#F2B134']
#     # Mgas-M
#     pr.plot_fit_mass_evol_dependence_params(
#             simdirs, insuffix_evol, kwargs, inprefix=inprefix_evol,
#             axes=["M500", "Mgas"],
#             param_kwargs={'ylims_slope': [0.95, 1.45],
#                           'normalise': False,
#                           'add_titles': True,
#                           'min_num': 10, 'col': col,
#                           'tspacing': {'norm': 0.1,
#                                        'scat': 0.05}},
#             minz=minz, maxz=maxz, zstep=zstep,
#             outdir=outdir)
#     # M-Tmw
#     pr.plot_fit_mass_evol_dependence_params(
#             simdirs, insuffix_evol, kwargs, inprefix=inprefix_evol,
#             axes=["T500mw", "M500"],
#             param_kwargs={'ylims_slope': [1.45, 1.9],
#                           'ylims_norm': [0.05, 0.13],
#                           'normalise': False,
#                           'min_num': 10, 'col': col,
#                           'tspacing': {'slope': 0.2,
#                                        'norm': 0.1,
#                                        'scat': 0.02}},
#             minz=minz, maxz=maxz, zstep=zstep,
#             outdir=outdir)
# =============================================================================

# =============================================================================
#     # fitting method dependence
# #    z = 0.4
# #    filenames = [inprefix_evol+"_z"+"{:.1f}".format(z)+insuffix_evol+".hdf5"]
# #    pr.plot_fit_method_dependence(simdirs, filenames, kwargs, z=z,
# #                                  axes=["T500mw", "mass"],
# #                                  plotlabel="z $="+"{:.1f}".format(z)+"$",
# #                                  outdir=outdir)
#     # Mgas-M
#     pr.plot_fit_method_dependence_params(
#             simdirs, insuffix_evol, kwargs, inprefix=inprefix_evol,
#             axes=["mass", "gasmass"],
#             param_kwargs={'ylims_slope': [0.95, 1.45],
#                           'normalise': False,
#                           'add_titles': True,
#                           'min_num': 10,
#                           'tspacing': {'norm': 0.1,
#                                        'scat': 0.05}},
#             minz=minz, maxz=maxz, zstep=zstep,
#             outdir=outdir)
#     # M-Tmw
#     pr.plot_fit_method_dependence_params(
#             simdirs, insuffix_evol, kwargs, inprefix=inprefix_evol,
#             axes=["T500mw", "mass"],
#             param_kwargs={'ylims_slope': [1.45, 1.9],
#                           'normalise': False,
#                           'min_num': 10,
#                           'tspacing': {'slope': 0.2,
#                                        'norm': 0.1,
#                                        'scat': 0.02}},
#             minz=minz, maxz=maxz, zstep=zstep,
#             outdir=outdir)
# =============================================================================

    # ----- SZ relations ----- #
    kwargs['basedir'] = "/data/curie4/nh444/project1/SZ/"
    kwargs['zoomdirs'] = ["/data/curie4/nh444/project1/SZ/MDRInt/"]
    # projected = True  # projected aperture, otherwise 3D spherical.

# =============================================================================
#     # Y500
#     kwargs['SZ_obs'] = {'Marrone12': False,
#                         'Wang': True,
#                         'Andersson11': True,
#                         'Planck14': True,
#                         'Nagarajan18': True,
#                         'Lim18': True,
#                         }
#     kwargs['projected'] = False  # projected aperture, otherwise 3D spherical.
#     kwargs['labels'] = ["FABLE"]
#     if kwargs['projected']:
#         insuffix = "_r500_proj"
#     else:
#         insuffix = "_r500"
# 
#     filenames = ["SZ-M500_z0.00"+insuffix+".hdf5"]
#     outname = "Y-M_z0.0.pdf"
#     kwargs['fit_lw'] = 4
#     fit_col = kwargs['fit_col']
#     kwargs['fit_col'] = [col[0]]  # set colour of fit to same as points
#     pr.plot_relation(simdirs, filenames, ["mass", "Y500"], z_comp=0.0,
#                      outname=outname, plot_sims=True,
#                      axeslims=[[7e12, 2e15], [1e-8, 0.002]],
#                      leg_len=5,
#                      **kwargs)
#     kwargs['fit_lw'] = lw
#     kwargs['fit_col'] = fit_col
# =============================================================================

# =============================================================================
#     # define kwargs for legend
#     leg_kwargs = {'slope': {'fontsize': 'medium',
#                             'borderaxespad': 1}}
#     pr.plot_relation_redshift(simdirs, ["mass", "Y500"], kwargs,
#                               inprefix="SZ-M500", insuffix=insuffix,
#                               outprefix="Y-M" if plot_fits else None,
#                               outsuffix="",
#                               minz=minz, maxz=maxz, zstep=zstep,
#                               param_kwargs={
#                                   'normalise': False,
#                                   'add_titles': True,
#                                   'ylims_slope': [1.2, None],
#                                   'ylims_scat': [None, 0.15],
#                                   'leg_kwargs': leg_kwargs,
#                                   'n_split': 3},
#                               plot_fits=plot_fits)
# =============================================================================

# =============================================================================
#     # Planck
#     kwargs['SZ_obs'] = {'Planck15': True, 'PlanckLBG': False,
#                         'Wang': True, 'SPT': False}
#     kwargs['labels'] = [None]
#     kwargs['projected'] = False
#     if kwargs['projected']:
#         insuffix = "_5r500_proj"
#     else:
#         insuffix = "_5r500"
#     pr.plot_relation_multi(simdirs, [0.0, 0.2, 0.4, 0.6, 0.8],
#                            ["mass", "Y5r500"], kwargs,
#                            "SZ-M500_vs_z_Planck.pdf", inprefix="SZ-M500",
#                            insuffix=insuffix)
# #    pr.plot_relation_redshift(simdirs, ["mass", "Y5r500"], kwargs,
# #                              inprefix="SZ-M500", insuffix=insuffix,
# #                              outprefix="Y-M" if plot_fits else None,
# #                              outsuffix="_Planck",
# #                              param_kwargs={'normalise': False},
# #                              minz=minz, maxz=maxz, zstep=zstep,
# #                              plot_fits=plot_fits)
# #    pr.plot_scatter(simdirs, [0.0, 0.4], ["mass", "Y5r500"], kwargs,
# #                    "scatter_SZ-M500_SPT.pdf", inprefix="SZ-M500",
# #                    insuffix=insuffix, plot_rel=True)
# =============================================================================

# =============================================================================
#     # SPT-SZ
#     kwargs['SZ_obs'] = {'Planck15': False, 'PlanckLBG': False,
#                         'Wang': False, 'SPT': True}
#     kwargs['projected'] = True
#     kwargs['axeslims'] = [[1e13, 3e15], None]
#     if kwargs['projected']:
#         insuffix = "_SPT_proj"
#     else:
#         insuffix = "_SPT"
#     pr.plot_relation_multi(simdirs, [0.2, 0.4, 0.6, 0.8, 1.0, 1.2],
#                            ["mass", "YSPT"], kwargs,
#                            "SZ-M500_vs_z_SPT.pdf", inprefix="SZ-M500",
#                            insuffix=insuffix)
# #    pr.plot_relation_redshift(simdirs, ["mass", "YSPT"], kwargs,
# #                              inprefix="SZ-M500", insuffix=insuffix,
# #                              outprefix="Y-M" if plot_fits else None,
# #                              outsuffix="_SPT",
# #                              param_kwargs={'normalise': False},
# #                              minz=max(0.2, minz), maxz=maxz, zstep=zstep,
# #                              plot_fits=plot_fits)
# 
# =============================================================================

if __name__ == "__main__":
    main()
