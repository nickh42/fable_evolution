#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 17:56:11 2018

@author: nh444
"""
import numpy as np
import stars.plot_stellar_mass as psm

outdir = "/home/nh444/Documents/Fable_evol/"
#outdir = "/data/curie4/nh444/project1/stars/"

# where stellar masses have been saved.
datadir = "/data/curie4/nh444/project1/stars/"
basedir = "/data/curie4/nh444/project1/boxes/"  # directory of simdirs
simdirs = ["L40_512_MDRIntRadioEff/"]
labels = ["FABLE"]
zoombasedir = "/home/nh444/data/project1/zoom_ins/"
zoomdirs = ["MDRInt/"]
zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
          "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
          "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
          "c512_MDRInt",
          "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
          "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
          "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
          "c464_MDRInt",
          "c496_MDRInt_SUBF",
          ]]
z = 0.0
h_scale = 0.6774

col = ['#24439b', '#ED7600']  # blue orange
ms = 8
mew = 1.8
errlw = 1.4
hlightcol = '#8292BF'


# =============================================================================
# # Calculate stellar masses.
# snapnums = range(59, 100, 4)[::-1]
# from stars.stellar_mass import stellar_mass
# #for simdir in simdirs:
# #    mstar = stellar_mass(basedir, simdir, datadir, snapnums, obs_dir, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=False)
# #    mstar.get_stellar_masses(M500min=10**11.6, verbose=True)
# for i, zoomdir in enumerate(zoomdirs):
#     if zoomdir is not "":
#         for zoom in zooms[i]:
#             mstar = stellar_mass(zoombasedir+zoomdir, zoom+"/",
#                                  "/data/curie4/nh444/project1/stars/"+zoomdir,
#                                  snapnums,
#                                  "/data/vault/nh444/ObsData/",
#                                  delta=500.0, ref="crit", h_scale=h_scale,
#                                  use_fof=True, masstab=False,
#                                  mpc_units=True)
#             mstar.get_stellar_masses(M500min=10**11.6, RminLowRes=5.0,
#                                      get_BCG_descendant=False, verbose=True)
# =============================================================================

#psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms, z, incl_ICL=True,
#                      projected=False, verbose=True, outdir=outdir,
#                      scat_idx=[0, 2], plot_sims=True, for_paper=True,
#                      labels=labels,
#                      col=col, ms=ms, mew=mew, errlw=0,
#                      hlightcol=hlightcol, h_scale=h_scale)

redshifts = np.arange(0, 1.5, 0.2)
psm.plot_mstar_v_redshift(datadir, simdirs, zoomdirs, zooms, redshifts,
                          M500min=2.9e14, outdir=outdir, scat_idx=[0],
                          for_paper=True, labels=labels,
                          col=col, hlightcol=hlightcol,
                          ms=ms, mew=mew, errlw=errlw, h_scale=h_scale)

#psm.plot_mBCG_v_mass(datadir, simdirs, zoomdirs, zooms, z=0,
#                 projected=True, for_paper=True, scat_idx=[0], labels=labels,
#                 apsize=30, outfilename="BCG_mass_vs_halo_mass_30kpc.pdf",
#                 outdir=outdir, col=col, hlightcol=hlightcol,
#                 errlw=errlw, ms=ms, mew=mew,
#                 plot_med=False, h_scale=h_scale, verbose=True)
#psm.plot_mBCG_v_mass(datadir, simdirs, zoomdirs, zooms, z=0,
#                 projected=True, for_paper=True, scat_idx=[0], labels=labels,
#                 apsize=50, outfilename="BCG_mass_vs_halo_mass_50kpc.pdf",
#                 outdir=outdir, col=col, hlightcol=hlightcol,
#                 errlw=errlw, ms=ms, mew=mew,
#                 plot_med=False, h_scale=h_scale, verbose=True)
#psm.plot_mBCG_v_mass(datadir, simdirs, zoomdirs, zooms, z=0,
#                 projected=True, for_paper=True, scat_idx=[0], labels=labels,
#                 apsize=100, outfilename="BCG_mass_vs_halo_mass_100kpc.pdf",
#                 outdir=outdir, col=col, hlightcol=hlightcol,
#                 errlw=errlw, ms=ms, mew=mew,
#                 plot_med=False, h_scale=h_scale, verbose=True)

#redshifts = np.arange(0, 1.5, 0.2)
#psm.plot_mBCG_v_redshift(datadir, simdirs, zoomdirs, zooms, redshifts,
#                         labels=labels, apsize=30, projected=True,
#                         M200lim=[3e13, 2e15], scale_by_mass=True,
#                         join_points=False, for_paper=True,
#                         figsize=(14, 7), ylims=(5e10, 5e12),
#                         outfilename="BCG_mass_vs_redshift_30kpc.pdf",
#                         outdir=outdir, scat_idx=[0], med_idx=[0],
#                         med_col='#ED7600',
#                         col=col, hlightcol=hlightcol,
#                         ms=ms, mew=mew, errlw=errlw,
#                         h_scale=h_scale, verbose=False)
#redshifts = np.arange(0, 2.0, 0.2)
#psm.plot_mBCG_v_redshift(datadir, simdirs, zoomdirs, zooms,
#                         redshifts, apsize=50, projected=True,
##                         M500lim=[3e13, 9e14],
#                         M500lim=[1e14, 1e16],
#                         join_points=False, for_paper=True,
#                         outfilename="BCG_mass_vs_redshift_50kpc.pdf",
#                         outdir=outdir, scat_idx=[0], labels=labels,
#                         col=col, hlightcol=hlightcol,
#                         ms=ms, mew=mew, errlw=errlw,
#                         h_scale=h_scale, verbose=False)
