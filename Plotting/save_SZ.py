#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 12:17:28 2018

@author: nh444
"""


def main():
    import numpy as np
    import scaling.SZ_scaling2 as SZ

    compare={'Planck15': False, 'PlanckLBG': False, 'SPT': False}

    outdir="/data/curie4/nh444/project1/SZ/"

    simdir = "/data/curie4/nh444/project1/boxes/"    
    simdirs = ["L40_512_MDRIntRadioEff"]
    labels = [None]
    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    zoomdirs = ["MDRInt/"] ## one for each in simdirs
    zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
              "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
              "c352_MDRInt", "c384_MDRInt", "c448_MDRInt", "c480_MDRInt",
              "c512_MDRInt"]]
    snapnums = range(15, 26)
    zooms = [["c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
              "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
              "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
              "c464_MDRInt", "c496_MDRInt_SUBF",
              ]]
    snapnums = range(59, 100, 4)

    Rlims=[0.0, 5.0]
    Rlimtype="R500"
    suffix = "_5r500"#"_5r500_fullbox_lowres"
    compare['Planck15'] = True
    compare['PlanckLBG'] = True
    ylims = [5e-8, 1e-3]
    z, tol = np.arange(0.0, 2.1, 0.2), 0.02
    outsuffix="_Planck"
    projected = False

#    Rlims=[0.0, 0.75]
#    Rlimtype="arcmin"
#    suffix = "_SPT"#"_SPT_fullbox_lowres"
#    compare['SPT'] = True
#    ylims = [1e-8, 1e-4]
#    z, tol = np.arange(0.2, 2.1, 0.2), 0.02
#    outsuffix = "_SPT"
#    projected = True

#    Rlims=[0.0, 1.0]
#    Rlimtype="R500"
#    suffix = "_r500"
#    projected = False

    h_scale = 0.6774 #0.7#value of h to scale output values and observation values to
    omega_m = 0.3089 #0.2793 #value of omega matter to scale output values and observation values to
    zthick = 0  # set to 0 for whole box, otherwise units of R500 e.g. 10
    # for 0.75' aperture the results at z=0.2 are indistinguishable
    # between whole box and 10*r500
    temp_thresh = -1
    filter_type = "none"
    M500min = 3e12 * h_scale

#    col = ['#24439b', '#ED7600'] ## blue orange
#    mew=1.8
#    ms=8

    for snapnum in snapnums:
#        SZ.save_SZ_M500(simdir, simdirs, snapnum, M500min=M500min,
#                        Rlims=Rlims, Rlimtype=Rlimtype,
#                        projected=projected, zthick=zthick,
#                        temp_thresh=temp_thresh, filter_type=filter_type,
#                        suffix=suffix)
        for i, zoomdir in enumerate(zoomdirs):
            if zoomdir is not "":
                SZ.save_SZ_M500(zoombasedir+zoomdir, zooms[i], snapnum,
                                highres_only=True, mpc_units=True,
                                M500min=M500min,
                                Rlims=Rlims, Rlimtype=Rlimtype,
                                projected=projected, zthick=zthick,
                                outdir=outdir+zoomdir,
                                temp_thresh=temp_thresh,
                                filter_type=filter_type,
                                suffix=suffix)
#    z, tol = [0.0], 0.2
#    for zi in z:
#        SZ.plot_SZ_M500(simdirs, zi, tol=tol, for_paper=True, title="", scat_idx=range(len(simdirs)), plot_5r500=True, med_idx=[], 
#                     M500min=1e13, ylims=ylims, compare=compare, zoomdirs=zoomdirs, zooms=zooms, projected=projected, outsuffix=outsuffix, insuffix=suffix, labels=labels, col=col, mew=mew, ms=ms, h_scale=h_scale, omega_m=omega_m, outdir=outdir)
#    
#    SZ.plot_SZ_M500_multi(simdirs, np.arange(0.2, 0.9, 0.2), tol=tol, outsuffix=outsuffix, insuffix=suffix, M500min=1e13, ylims=ylims,
#                       do_fit=True, compare=compare, zoomdirs=zoomdirs, zooms=zooms, projected=projected,
#                       labels=labels, col=col, mew=mew, ms=ms, h_scale=h_scale, omega_m=omega_m, outdir=outdir)

#    SZ.plot_SZ_M500_redshift(simdirs, outsuffix=outsuffix, insuffix=suffix,
#                             M500min=1e13, ylims=ylims,
#                       zoomdirs=zoomdirs, zooms=zooms, projected=projected,
#                       compare=compare, h_scale=h_scale, omega_m=omega_m, outdir=outdir)

if __name__=="__main__":
    main()    