#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 13:46:31 2019

@author: nh444
"""
# self-similar evolution
gamma_LM = 7.0/3.0
gamma_TM = 2.0/3.0

# self-similar slopes
beta_LM, beta_LM_err = 4.0/3.0, 0.0
beta_TM, beta_TM_err = 2.0/3.0, 0.0

## XXL from Maughan 2014
#beta_LM, beta_LM_err = 1.64, 0.09
#beta_TM, beta_TM_err = 0.57, 0.03
#
## FABLE z = 0
beta_LM, beta_LM_err = 1.719, 0.09
beta_TM, beta_TM_err = 1.0 / (1.699), 0.04 / 1.699**2  # mass-weighted
gamma_LM = 3.0
gamma_TM = 0.9*beta_TM  # gamma_TM = -gamma_MT * beta_TM
# Note that this makes beta_TM irrelevant.

gamma_LT = gamma_LM - (beta_LM / beta_TM) * gamma_TM

gamma_LT_err = gamma_LT * ((beta_TM_err / beta_TM)**2 + (beta_LM_err / beta_LM)**2)**0.5

print "gamma_LT (err) = {:.3f} ({:.3f})".format(gamma_LT, gamma_LT_err)
