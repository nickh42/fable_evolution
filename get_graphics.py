# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 17:31:45 2019

@author: Groovitron
"""
import sys
import re

filename = str(sys.argv[1])

with open(filename, 'r') as f:
    content = f.read()
    results = re.findall(r'\\includegraphics.*\{.*\}', content)
print(" ".join([re.sub(r'\\includegraphics.*\{|\}', '', result) for result in results]))
#for result in results:
#    print(re.sub(r'\\includegraphics.*\{|\}', '', result))
